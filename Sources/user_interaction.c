#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#define WIDTH 8
#define HEIGHT 5

void print_tile(int x, int y, int id, int rot, int *tiles){
    //Function creating a tile with corresponding parameters
    // tiles is supposed to be an array made from tiles.txt provided by another function
    x = x * WIDTH + 3;
    y = y * HEIGHT + 1;
    
    WINDOW *tile;
    tile = newwin(HEIGHT,WIDTH,y,x);
    box(tile,0,0);

    //just a test
    mvwprintw(tile,2,3,"TL");
    mvwprintw(tile,3,3,"||");
    wrefresh(tile);
}

void print_coords(int x_off, int y_off){
    //prints grid enumeration
    char x_co[6],y_co[6];
    for(int i=0;i<8;i++){
        sprintf(x_co,"%d",i+x_off);
        sprintf(y_co,"%d",i+y_off);
        mvprintw(0,i*WIDTH+6,x_co);
        mvprintw(i*HEIGHT+1+2,0,y_co);
    }
}
    
void update_board(int x_off, int y_off){
    // updates the state of board displayed in terminal
    // later it will take, tiles and map as an argument
    for(int x=0;x<8;x++){
        for(int y=0;y<8;y++){
            print_tile(x,y,0,0,0);
        }
    }
}

void display_legend(){
    mvprintw(49,0,"Press p to quit");
    mvprintw(48,0,"TL - Temple");
    mvprintw(47,0,"CR - Crossroads");
    mvprintw(46,0,"||,== - Roads");
    mvprintw(45,0,"w,s,a,d to scroll the map");
}

void display_menu(){
    ;
}


// just for it to work indepentedly
int main(int argc, char *argv[]) {
    int x_off=0, y_off=0, input,input2;
    int av_tiles[64]; // contains tile ids that can be used, 
    int tiles[128][8]; // contains information about tiles "dictionary" almost
    
    system("printf '\e[8;50;75t'"); //changes size of terminal
    
    initscr(); // initiates ncurses screen
    noecho();  // turns off verbose input
    refresh(); // refreshes screen
    
    display_menu(); // not yet
    
    while(true){
        update_board(x_off,y_off);
        display_legend();
        print_coords(x_off,y_off);
        
        input = getch();
        
        if(input == 'w')
            y_off--;
        if(input == 's')
            y_off++;
        if(input == 'a')
            x_off--;
        if(input == 'd')
            x_off++;
        if(input == 'p'){
            mvprintw(49,0,"Are you sure you want to quit?(Y/N)");
            refresh();
            input = getch();
            if(input == 'y')
                break;
        }
        
        clear();
        refresh();
        // Ends if av_tiles is empty
    }
    
    endwin();
    
    return 0;
    
}

/*
 *  Display menu, modes?  single, vs, vs pc
 *  Display legend,
 *  turns looping: display av_tiles with ids, display map, ask for tile_id(error handling), ask for place
 *
 */

