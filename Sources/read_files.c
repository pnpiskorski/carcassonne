#define MAX_LENGTH 100
#define TILE_LENGTH 5

//this function converts data from string to structure
TILE strToStruct(char *string )
{
    TILE temp;

    temp.id = atoi( strtok(string,",") );

    strcmp(temp.name,strtok(NULL,","));

    temp.isTemple = atoi( strtok(NULL,",") );

    temp.crossRoads = atoi( strtok(NULL,",") );

    temp.bonus = atoi( strtok(NULL,",") );

    temp.top = atoi( strtok(NULL,",") );

    temp.right = atoi( strtok(NULL,",") );

    temp.bot = atoi( strtok(NULL,",") );

    temp.left = atoi( strtok(NULL,",") );

    return temp;

}

//this function reads file to structure
TILE TILE_fileToStructure(FILE *ftiles) {

    char *tiles = 0;
    long length;

//copying file to string
    if (ftiles)
    {
        fseek(ftiles,0,SEEK_END);
        length=ftell(ftiles);
        fseek(ftiles,0,SEEK_SET);
        tiles=malloc(length);
        if (tiles)
        {
            fread(tiles,1,length,ftiles);
        }
    }

    char *temp;
    int i;
    char tile[MAX_LENGTH][MAX_LENGTH];

//splitting string
    temp=strtok(tiles,";");
    for(i=0; temp!=NULL ;i++)
    {
        stpcpy(tile[i],temp);
        temp=strtok(NULL,";");
    }
    int lines=i;


//converting strings to structures
    TILE stiles[lines];

    for(i=0;i<lines;i++)
    {
        stiles[i]=strToStruct(tile[i]);
    }
// first tile -> stiles[0] , second tile -> stiles[1] and so on ... 14th tile -> stiles[13]


    return stiles;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//this function converts data from string to structure
MTILE strToStruct2(char *string,int i,int j)
{
    MTILE temp;

    if( strcmp("",string) == 0 )
    {

        temp.x = 0;

        temp.y = 0;

        temp.id  = 0;

        temp.rot = 0;
    }
    else
        {

        temp.x = i;

        temp.y = j;

        temp.id = atoi(strsep(&string, "_"));

        temp.rot = atoi(string);
    }

    return temp;
}
//this function reads file to structure
MTILE MTILE_fileToStructure(FILE *fmap;) {

    char *map = 0;
    long length;

//copying file to string
    if (fmap)
    {
        fseek(fmap,0,SEEK_END);
        length=ftell(fmap);
        fseek(fmap,0,SEEK_SET);
        map=malloc(length);
        if (map)
        {
            fread(map,1,length,fmap);
        }
    }

    char *temp1;
    char *temp2;
    int i,j;
    char strmap[MAX_LENGTH][MAX_LENGTH][TILE_LENGTH];

//splitting string by rows and columns

    for(i=0; (temp1 = strsep(&map,"\n")) !=NULL ;i++)
    {
        for(j=0; (temp2=strsep(&temp1,",")) !=NULL ;j++)
        {
            strcpy(strmap[i][j],temp2);
        }
    }
    int row = i;
    int col = j;

//converting strings to structures
    MTILE mtile[row][col];

    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
            mtile[i][j]=(strToStruct2(strmap[i][j],i,j));
    }

    return mtile;
}