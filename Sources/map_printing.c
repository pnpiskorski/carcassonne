#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TBROAD "||"
#define LRROAD '='
#define TEMPLE "TL"
#define CITY 'C'
#define BONUS 'b'
#define CROSS "XX"

// should be someplace else
// tiles structure containing all information about tile
typedef struct{
    int id;
    int isTemple;
    int crossRoads;
    int bonus;
    int top;
    int right;
    int bot;
    int left;
}TILE;

// should be someplace else
// structure for map
typedef struct{
    int x;
    int y;
    int id;
    int rot;
}MTILE;

/*
 *  adds crossing lines to the 2d board array
 *  takes 2d board array pointer
 */
void make_lines(char *board){
    // makes the chessboard like styling
    int i,j;
    for(i=0;i<34;i++){
        if(i%4==1){
            for(j=0;j<61;j++){
                *(board+j+i*61) = '-';
            }
        }
        else{
            for(j=0;j<61;j++){
                if(j%7==4){
                    *(board+j+i*61) = '|';
                }
                else{
                    *(board+j+i*61) = ' ';
                }
            }
        }
    }
}

/*
 *  inserts coordinate numbers to board 2d array
 *  takes 2d array of board pointer and x,y offsets
 */
void make_coords(char *board, int x_off, int y_off){
    int i,j;
    char temp1[4] = {0}, temp2[6] = {0};
    for(i=0;i<8;i++){
        sprintf(temp1,"%d",i+y_off);
        sprintf(temp2,"%d",i+x_off);
        for(j=0;j<4;j++){
            if(temp1[j]=='\0')
                *(board+(3+i*4)*61+j) = ' ';
            else
                *(board+(3+i*4)*61+j) = temp1[j];
        }
        for(j=0;j<5;j++){
            if(temp2[j]=='\0'){
                *(board+6+i*7+j) = ' ';
                
            }
            else{
                *(board+6+i*7+j) = temp2[j];
            }
        }
    }
}

/*  !!!should be somewhere else probably!!!
 *  rotates tile structure
 *  takes tile pointer and times it was rotated clockwise
 */
void rotate_tile(TILE* tile, int rot){
    int temp,i;
    for(i=0;i<rot;i++){
        temp = (*tile).top;
        (*tile).top = (*tile).left;
        (*tile).left = (*tile).bot;
        (*tile).bot = (*tile).right;
        (*tile).right = temp;
    }
}

/*
 *  changes an empty 3x6 char array so that it matches tile structure
 *  takes 3x6 char array, id and rotation of a tile and tiles "dictionary and it's size
 */
void make_tile(char *tile,int id, int rot, TILE *tiles, int s_t){
    TILE temp;
    int i;

    for(i=0;i<s_t;i++){
        if((*(tiles+i)).id == id){
            temp = *(tiles+i);
            break;
        }
    }
    rotate_tile(&temp,rot);
    
    for(i=0;i<18;i++){
        *(tile+i) = ' ';
    }
    if(temp.bonus==1)
        *(tile + 2*6 + 5) = BONUS;
    if(temp.isTemple == 1){
        *(tile + 1*6 + 2) = TEMPLE[0];
        *(tile + 1*6 + 3) = TEMPLE[1];
    }
    if(temp.crossRoads == 1){
        *(tile + 1*6 + 2) = CROSS[0];
        *(tile + 1*6 + 3) = CROSS[1];
    }
    switch (temp.top) {
        case 1:
            *(tile + 2) = TBROAD[0];
            *(tile + 3) = TBROAD[1];
            break;
        case 2:
            *(tile + 2) = CITY;
            *(tile + 3) = CITY;
            break;
    }
    switch (temp.right) {
        case 1:
            *(tile + 6 + 4) = LRROAD;
            *(tile + 6 + 5) = LRROAD;
            break;
        case 2:
            *(tile + 6 + 5) = CITY;
            break;
    }
    switch (temp.bot) {
        case 1:
            *(tile + 12 + 2) = TBROAD[0];
            *(tile + 12 + 3) = TBROAD[1];
            break;
        case 2:
            *(tile + 12 + 2) = CITY;
            *(tile + 12 + 3) = CITY;
            break;
    }
    switch (temp.left) {
        case 1:
            *(tile + 6) = LRROAD;
            *(tile + 6 + 1) = LRROAD;
            break;
        case 2:
            *(tile + 6) = CITY;
            break;
    }
}

/*
 *  inserts text to a string
 *  takes string to be inserted in, insertion, position and size of insertion
 */
void insert_into_string(char *string, char insertion[], int pos, int size){
    int i;
    for(i=0;i<size;i++){
        if(insertion[i] != '\0')
            *(string+pos+i) = insertion[i];
    }
}
/*
 *  inserts a tile to a 2d array
 *  tekes 2d array with the board, 2d array of tile, x and y position for insertion
 */
void insert_tile(char *board, char *tile, int posx, int posy){
    int i,j;
    // pos x = 2+4*i
    for(i=0;i<3;i++){
        for(j=0;j<6;j++){
            *(board+(2+4*posy+i)*61+7*posx+5+j) = *(tile+i*6+j) ;
        }
    }
}

/*
 *  Prints board on a screen
 *  takes tiles dictionary, size of it, map, size of it, x and y offsets
 */
void print_board(TILE* tiles,int s_t, MTILE* mtiles, int s_mt, int x_off, int y_off){
    char board[34][61] = {0}, tile[3][6] = {0};
    int i,j,no_t = 2;
    int id=0;
    make_lines(&board[0][0]);
    make_coords(&board[0][0],x_off,y_off);
    
    // checks for tiles that are in range of current board view
    for(i=0;i<s_mt;i++){
        if((*(mtiles+i)).x >= x_off && (*(mtiles+i)).x <= x_off+7 && (*(mtiles+i)).y >= y_off && (*(mtiles+i)).y <= y_off+7){
            make_tile(&tile[0][0],(*(mtiles+i)).id,(*(mtiles+i)).rot,tiles,s_t);
            insert_tile(&board[0][0],&tile[0][0],(*(mtiles+i)).x - x_off,(*(mtiles+i)).y - y_off);
        }
    }
    printf("%c",'\n');
    for(i=0;i<34;i++){
        for(j=0;j<61;j++)
            printf("%c",board[i][j]);
        printf("\n");
    }
}
/*
 *  Print available tiles on screen in rows
 *  Takes tiles "dictionary", size of it, array of available tiles
 */
void show_tiles(TILE *tiles, int s_t, int av_tiles[]){
    // takes list of tiles, list of available tiles, and number of tiles in list of tiles
    int i=0,j,k,rows,no_t=0;
    char p[7][61] = {0};
    char tile[3][6] = {0};
    char temp[5] = {0};
    
    // counting ids in av_tiles
    while(1){
        if(av_tiles[i] == 0)
            break;
        no_t++;
        i++;
    }
    rows = no_t/8 + 1;
    
    printf("Available tiles:\n");
    // printing available tiles in borders, similiar to board printing
    for(k=0;k<rows;k++){
        for(i=1;i<7;i++){
            for(j=0;j<61;j++){
                if(i==1 || i==5)
                    p[i][j] = '-';
                else if(i!=5 && i!=0){
                    if(j%7 == 4)
                        p[i][j] = '|';
                    else
                        p[i][j] = ' ';
                }
            }
        }
        
        insert_into_string(&p[6][0],"id:", 0,3);
        
        for(i=0;i<8;i++){
            if(av_tiles[k*8+i]==0)
                break;
            make_tile(&tile[0][0], av_tiles[k*8+i], 0, tiles,s_t);
            insert_tile(&p[0][0], &tile[0][0], i, 0);
            
            sprintf(temp,"%d",av_tiles[k*8+i]);
            insert_into_string(&p[6][0],temp, 6+i*7,5);
        }
        
        for(i=0;i<6;i++){
            for(j=0;j<61;j++)
                printf("%c",p[i+1][j]);
            printf("%c",'\n');
        }
     }
}

/*
 *  function checks if ids that are used in arrays of available tiles and map are defined intiles "dictionary"
 *  takes tiles "dictionary", size of it, available_tiles array, map array, size of it
 *  returns error codes, -1 - problem with av_tiles, -2 problem with map, 0 if no problems
 */
int check_for_errors(TILE *tiles, int s_t, int av_tiles[], MTILE *mtiles, int n_mt){
    int i=0,j;
    int c_t = 0;
    int error = 1;
    while(1){
        if(av_tiles[i] == 0)
            break;
        c_t++;
        i++;
    }
    // checking if av_tiles has all ids defined
    for(i=0;i<c_t;i++){
        error = 1;
        for(j=0;j<s_t;j++){
            if(av_tiles[i] == (*(tiles+j)).id){
                error = 0;
                break;
            }
        }
        if(error == 1)
            return -1;
    }
    // checking if mtiles has all ids defined
    for(i=0;i<n_mt;i++){
        error = 1;
        for(j=0;j<s_t;j++){
            if((*(mtiles+j)).id == (*(tiles+i)).id){
                error = 0;
                break;
            }
        }
        if(error == 1)
            return -2;
    }
    return 0;
}

// mainly for testing purposes
int main() {
    int av_tiles[32] ={1,2};
    int s_t = 0, s_mt = 0;
    int check = 0;
    TILE xd;
    TILE *tiles;
    
    MTILE temp;
    MTILE *mtiles;
    
    // memory allocation for tiles and mtiles
    tiles = malloc(sizeof(TILE)*20);
    mtiles = malloc(sizeof(MTILE)*20); //*number of tiles on the map
    
    // populating mtiles array containing id,rotation and coordinates of tiles on the map
    temp.id = 1;
    temp.rot = 3;
    temp.x = 1;
    temp.y = 1;
    *mtiles = temp;
    s_mt++;
    
    temp.id = 2;
    temp.rot = 0;
    temp.x = 2;
    temp.y = 1;
    *(mtiles+1) = temp;
    s_mt++;
    
    // populating tiles array containing information about tiles
    xd.id = 1;
    xd.isTemple = 1;
    xd.crossRoads = 0;
    xd.bonus = 0;
    xd.top = 0;
    xd.right = 0;
    xd.bot = 1;
    xd.left = 0;
    *(tiles) = xd;
    s_t++;
    
    xd.id = 2;
    xd.isTemple = 0;
    xd.crossRoads = 1;
    xd.bonus = 1;
    xd.top = 1;
    xd.right = 1;
    xd.bot = 1;
    xd.left = 1;
    *(tiles+1) = xd;
    s_t++;
    
    // error handling for files
    check = check_for_errors(tiles,s_t,av_tiles,mtiles,s_mt);
    if(check == -1){
        printf("Problem with av_tiles\n");
    }
    else if(check == -2){
        printf("Problem with mtiles\n");
    }
    else{
        // everything ok
        print_board(tiles,s_t,mtiles,s_mt,0,0);
        show_tiles(tiles,s_t,av_tiles);
    }
    // memory freeing
    free(tiles);
    free(mtiles);
    
    return 0;
}
